import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyDcgiBlMeHEu9KILFxPvn_RCeOWe1YzBTY',
  authDomain: 'lspunsil-c1643.firebaseapp.com',
  databaseURL: 'https://lspunsil-c1643-default-rtdb.firebaseio.com',
  projectId: 'lspunsil-c1643',
  storageBucket: 'lspunsil-c1643.appspot.com',
  messagingSenderId: '1093588028295'
};

firebase.initializeApp(config);

export default firebase;

export const database = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();