import React,{useEffect,useState} from 'react'
import {Table, Card} from 'reactstrap'
import TablesDetail from './TablesDetail'

const Tables = ({datas,sum}) => {

  const [dataTable1, setDataTable1] = useState([])
  const [key, setKey] = useState([])
  useEffect(()=>{
    groupBy(datas,'skema_sertifikasi').then(res=>setDataTable1(res))
  },[datas])


  function groupBy(collection, property) {
      return new Promise((resolve,reject)=>{
        var i = 0, val, index,
            values = [], result = [];
        for (; i < collection.length; i++) {
            val = collection[i][property];
            index = values.indexOf(val);
            if (index > -1)
                result[index].push(collection[i]);
            else {
                values.push(val);
                result.push([collection[i]]);
            }
          }
          resolve(result)
      })
    }
    
  return (
    <>
    <Card>
    <Table bordered responsive className="table-hover">
      <thead className="thead-dark">
        <tr>
          <th>No</th>
          <th>Nama Skema</th>
          <th>Jumlah</th>
          <th>Persentase</th>
        </tr>
      </thead>
      <tbody>
        {dataTable1.map((data,key)=>{
            return (
              <tr key={key}>
                <td>{key+1}</td>
                <td onClick={()=>{setKey(key)}} ><a href="#tableDetail">{data[0].skema_sertifikasi}</a> </td>
                <td>{data.length}</td>
                <td>{`${((data.length/sum)*100).toFixed(0)} %`}</td>
              </tr>
            )
          })
        }
        <tr>
          <td colSpan="2" className="text-center text-bold"><strong>Jumlah</strong></td>
          <td>{sum}</td>
          <td>{(sum/sum)*100}%</td>
        </tr>
      </tbody>
    </Table>
    </Card>
    <TablesDetail  key={key} data={dataTable1[key]}/>

    </>
  )
}

export default Tables