// data =
// alamat_perusahaan: ""
// alamat_rumah: "Perumahan Taman Jatisari Permai Jl. Melati 7 IA/08 RT 06 RW 17 Kelurahan Jatisari Kecamatan Jatiasih Kota Bekasi"
// email: "175001003@student.unsil.ac.id"
// foto: "https://firebasestorage.googleapis.com/v0/b/lspunsil-c1643.appspot.com/o/images%2FAnnisa%20Yasmin_175001003.jpeg?alt=media&token=6cfa1c76-5f6b-47a7-8425-d7ec3f3d8db9"
// jabatan: ""
// jenis_kelamin: "P"
// kabupaten_kota: {kode: "3275", nama: "Kota Bekasi"}
// kebangsaan: "Indonesia"
// nama: "Annisa Yasmin"
// nik: "3171071511990001"
// no_hp: "082353142271"
// no_telp_perusahaan: ""
// npm: "175001003"
// pendidikan_terakhir: {kode: "7", nama: "S1"}
// perusahaan: ""
// provinsi: {kode: "32", nama: "Jawa Barat"}
// skema_sertifikasi: "Fasilitator Pertanian Organik Tanaman"
// tanggal_lahir: "15/11/1999"
// tempat_lahir: "Jakarta"
// tujuan_assesment: "Sertifikasi"

import React, {useEffect,useState} from 'react'
import {Table, Card} from 'reactstrap'

const TableDetail = ({data})=>{
  const [datas,setDatas] = useState([])
  useEffect(() => {
    if (data) {
      setDatas(data)
    }
  }, [data])

 
  return(
    <>
    <Card className="p-2" id="tableDetail"> 
    <h2>{data ? `Daftar Skema Sertifikasi  ${data[0].skema_sertifikasi}` : 'Choose One Above'}</h2>
      <Table>
        <thead>
          <tr>
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
            <th>No Hp</th>
          </tr>
        </thead>
        <tbody>
          {datas.map((data,key) => {
            return(
              <tr>
                <td>{key+1}</td>
                <td>{data.npm}</td>
                <td>{data.nama}</td>
                <td>{data.no_hp}</td>
              </tr>
            )
          })}
        </tbody>
      </Table>

    </Card>
    </>
  )
}

export default TableDetail