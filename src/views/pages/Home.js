import React, {useEffect, useState} from "react"
import {database} from '../../firebase'
import Tables from './Tables'

const Home = ()=>{
  const [datas,setDatas] = useState([])
  
  // const [fixData, setFixData] = useState([])
  const db = database.ref('/form')
    useEffect(()=>{
      db.on('value',(snapshot)=>{
        const tempData =[]
        snapshot.forEach(snap=>{
          tempData.push(snap.val())
        })
        setDatas(tempData)
        
      })
      // const tempData = []
      // db.orderByChild('skema_sertifikasi').equalTo("Fasilitator Penyuluhan Pertanian Organik").on('child_added',snapshot => {
      //   tempData.push(snapshot.val())
      //   setDatas(tempData)
      // })
    },[])
   

    // const render = ()=>{
    //   if (datas) {
    //     let FPPO = datas.filter(data => data.skema_sertifikasi === "Fasilitator Penyuluhan Pertanian Organik") 
    //     let APP = datas.filter(data => data.skema_sertifikasi === "Advisor Penyuluh Pertanian")
    //     let FPP = datas.filter(data => data.skema_sertifikasi === "Fasilitator Penyuluhan Pertanian")
    //     let SPP = datas.filter(data => data.skema_sertifikasi === "Supervisor Penyuluh Pertanian")
    //     let FPOT = datas.filter(data => data.skema_sertifikasi === "Fasilitator Pertanian Organik Tanaman")
    //     let MPTIK = datas.filter(data => data.skema_sertifikasi === "Manajer Proyek TIK")
    //     let NAM = datas.filter(data => data.skema_sertifikasi === "Network Administrator Madya")
    //     let anPro = datas.filter(data => data.skema_sertifikasi === "Analis Program")
    //     let JWP = datas.filter(data => data.skema_sertifikasi === "Junior Web Programmer")
    //     console.log(FPPO.length + MPTIK.length + JWP.length + anPro.length + NAM.length + SPP.length + APP.length + FPOT.length + FPP.length)
    //     console.log(FPPO)
        
    //     console.log(datas.length)
    //   }else {
    //     console.log(datas)
     
    //   }
    // }

    return (<>
      <Tables datas={datas} sum={datas.length} />
    </>)
}

export default Home